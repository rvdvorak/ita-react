import React from 'react'
import ROUTES from '../routes'

import customersService from './customers-service'

import CustomerForm from './customer-form'

class CustomerEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  load(id) {
    customersService.getCustomer(id).then(res => {
      this.setState({
        customer: res.data
      })
    })
  }

  update() {
    customersService.updateCustomer(this.state.customer).then(res => {
      this.props.history.push(ROUTES.CUSTOMER_LISTING)
    })
  }

  handleChange(e) {
    this.setState({
      ...this.state,
      customer: {
        ...this.state.customer,
        [e.target.name]: e.target.value,
      }
    })
  }

  render() {
    const customer = this.state.customer

    return (
      <div className="box">
        <div className="box-header">
          <h3 className="box-title">Edit Customer</h3>
        </div>
        <div className="box-body">
          {customer && <CustomerForm customer={customer} handleChange={e => this.handleChange(e)} />}
        </div>
        <div className="box-footer">
          <button className="btn btn-primary" onClick={() => this.update()}>Save</button>
        </div> 
      </div>
    )
  }
}

export default CustomerEdit
