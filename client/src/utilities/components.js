import React from 'react';


export const Messages = ({infoMessage, errorMessage, dismissInfo, dismissError}) => 
  [
    errorMessage &&
      <div className="alert alert-danger" role="alert" key={1}>
        <div className="row">
          <div className="col-xs-9">
            {errorMessage}
          </div>
          <div className="col-xs-3">
            <div className="pull-right"><a href="/#" onClick={dismissError}>Got it.</a></div>
          </div>
        </div>
      </div>
  ,
    infoMessage &&
      <div className="alert alert-success" role="alert" key={2}>
        <div className="row">
          <div className="col-xs-9">
            {infoMessage} 
          </div>
          <div className="col-xs-3">
            <div className="pull-right"><a href="/#" onClick={dismissInfo}>Got it.</a></div>
          </div>
        </div>
      </div>
  ]
