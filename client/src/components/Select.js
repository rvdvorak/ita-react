import React from 'react';

export class Select extends React.Component {
  static defaultProps = {
    onChange() {},
  }
  
  render() {
    return (
      <select value={this.props.value} className={this.props.className} onChange={e => this.props.onChange(e.target.value)}>
        {this.props.items.map(o => 
          <option value={o.name} key={o.name}>{o.label}</option>
        )}
      </select>
    )
  }
}