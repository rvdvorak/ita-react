import React from 'react'

import dashboardService from './dashboard-service'

import funnelChartService from './funnel-chart-service'

import FunnelChart from './funnel-chart'

class Dashboard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customersCount: 0,
      contractsCount: 0,
      contactStatusData: []
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    dashboardService.getContractsCount().then(count => {
      this.setState({
        contractsCount: count
      })
    })

    dashboardService.getCustomersCount().then(count => {
      this.setState({
        customersCount: count
      })
    })

    this.setState({
      contactStatusData: await funnelChartService.getStatusData()
    })
  }

  render() {
    const customersCount = this.state.customersCount
    const contractsCount = this.state.contractsCount
    const contactStatusData = this.state.contactStatusData
    return (
      <div className="dashboard-count-boxes">
        <div className="box box-solid box-primary" data-widget="box-widget">
          <div className="box-header">
            <h3 className="box-title">Customers Count</h3>
          </div>
          <div className="box-body">
            Customers Count: {customersCount}
          </div>
        </div>
        <div className="box box-solid box-primary" data-widget="box-widget">
          <div className="box-header">
            <h3 className="box-title">Contracts Count</h3>
          </div>
          <div className="box-body">
            Contracts Count: {contractsCount}
          </div>
        </div>
        <div className="box box-solid box-primary" data-widget="box-widget">
          <FunnelChart contactStatusData={contactStatusData} />
        </div>
      </div>
    )
  }
}

export default Dashboard
