

import React from 'react';
import ReactDOM from 'react-dom';
import '../src/include/bootstrap'
import './index.css';
import App from './app';
import authService from './auth-service'

import './include/pace/theme/pace-theme-center-atom.css';
import 'pace-progress/pace';

window.Pace.start({
  ajax: {
    trackMethods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
  },
  target: '#pace-container'
});

const render = () =>
    ReactDOM.render(<App />, document.getElementById('root'));

authService.init()

render()
authService.onChange = render
//window.click = render
//window.oninput = render