import http from "../http"
import _ from "lodash"
import queryString from "query-string"
import { dateToIsoString } from "../format"

const ContractsService = {
  getContracts() {
    return http.get("/contracts?_expand=customer")
  },

  createContract(data) {
    data.dateCreated = dateToIsoString()
    return http.post("/contracts", data)
  },

  getContract(id) {
    return http.get(`/contracts/${id}?_expand=customer`)
  },

  filterContracts(filter) {
    return http.get(
      `/contracts?_expand=customer&${queryString.stringify(filter)}`
    )
  },

  updateContract(contract) {
    contract.dateEdited = dateToIsoString()
    const date = contract.deadline
    if (!date) {
      contract.deadline = ""
    }
    else {
      contract.deadline = new Date(date).toISOString()
    }
    return http.put("/contracts/" + contract.id, _.omit(contract, "customer"))
  },

  deleteContract(id) {
    return http.delete("/contracts/" + id)
  },

  searchContracts(searchText) {
    return http.get("/contracts?_expand=customer&q=" + searchText)
  },

  createContractComment(contractId, comment) {
    comment.contractId = contractId
    comment.date = dateToIsoString()
    return http.post("/comments/", comment)
  },

  getContractComments(contractId) {
    return http.get("/comments/?contractId=" + contractId)
  },

  getStatuses() {
    return http.get("/statuses")
  }
}

export default ContractsService
